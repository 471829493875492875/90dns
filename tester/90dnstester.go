/*
   90dnstester -- test the 90dns servers to make sure they work on your network
   written by pika, licensed under the gnu gpl
   you can grab a copy at https://www.gnu.org/licenses/gpl-3.0.en.html
*/

package main

import (
	"bufio"
	"fmt"
	"github.com/miekg/dns"
	"os"
)

func testRecord(server string, domain string, ip string) bool {
	c := new(dns.Client)
	m := new(dns.Msg)
	m.SetQuestion(dns.Fqdn(domain), dns.TypeA)
	r, _, err := c.Exchange(m, server+":53")
	if err != nil {
		fmt.Printf("[err] there was an error while sending the request to the dns server:\n")
		panic(err)
	}
	rr, _ := dns.NewRR(domain + ".   604800  IN      A       " + ip)
	if r.Answer[0].String() == rr.String() {
		return true
	} else {
		return false
	}
}

func main() {
	ips := []string{"163.172.141.219", "45.248.48.62"}
	testset := [][]string{
		[]string{
			"nintendo.com", "127.0.0.1",
		},
		[]string{
			"nintendo.net", "127.0.0.1",
		},
		[]string{
			"ctest.cdn.nintendo.net", "95.216.149.205", "45.248.48.62",
		},
		[]string{
			"conntest.nintendowifi.net", "95.216.149.205", "45.248.48.62",
		},
		[]string{
			"90dns.test", "95.216.149.205", "45.248.48.62",
		},
	}
	successfulTests := 0
	for _, dns := range ips {
		for _, test := range testset {
			if testRecord(dns, test[0], test[1]) {
				successfulTests += 1
				fmt.Printf("[info] test on %s with %s succeeded!\n", test[0], dns)
			} else if testRecord(dns, test[0], test[2]) {
				successfulTests += 1
				fmt.Printf("[info] test on %s with %s succeeded!\n", test[0], dns)
			} else {
				fmt.Printf("[info] test on %s with %s failed!\n", test[0], dns)
			}
		}
	}

	if successfulTests == len(testset)*2 {
		fmt.Printf("[info] %d/%d tests succeeded! it is safe to use 90dns on this network!\n", successfulTests, len(testset)*2)
	} else {
		fmt.Printf("[info] %d/%d tests succeeded! it is *not* safe to use 90dns on this network!\n", successfulTests, len(testset)*2)
	}

	fmt.Printf("[info] press enter to exit.")
	bufio.NewScanner(os.Stdin).Scan()
}
